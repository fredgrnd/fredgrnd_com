---
title: "Curriculum Vitae"
description: Qui je suis ? Qu'est ce que je fais ?
menu: hidden
date: 2018-04-19T13:38:58+04:00
comments: false

---
## Consultant en architectures Cloud et DevOPS - FredGRND Consulting

* Conseil et architecture Cloud (AWS, Azure)
* Automatisation (Ansible, Terraform)
* DevOPS
* Infogérence
* Prestations sur site et en remote

## Anciens postes

### Ingénieur IT/Tech Lead - Linkbynet Maurice de Juillet 2017 - Juillet 2018

* **Référent client NISSAN et PCIS**
* Conseil et cadrage des demandes clients
* Formation des administrateurs sur les plateforme client
* Assurer la santé et l’évolution des plateforme
* Sécurité
* **Référent technique applicatif (Varnish, MySQL, AppDynamics)**
* Support N2 sur les applications
* Rédaction de RFC
* Veille sur les nouvelles versions
* **Formations**
* Formation des nouveaux collaborateurs
* Formation sur les techniques DevOPS (Docker, Terraform Ansible)

### Ingénieur IT - Linkbynet Nantes de Juin 2012 - Juillet 2017

* **Référent client NISSAN et PCIS**
* Conseil et cadrage des demandes clients
* Formation des administrateurs sur les plateforme client
* Assurer la santé et l’évolution des plateforme
* Sécurité
* **Référent technique applicatif (Varnish, MySQL, AppDynamics)**
* Support N2 sur les applications
* Rédaction de RFC
* Veille sur les nouvelles versions

***

### Expert Technique - GFI Nantes de Avril 2010 à Juin 2012

* Réalisation d’audit sur les technologies de cluster Red Hat.
* Réalisation d’une solution de PCA avec prise en compte de facilité de déploiement. Evolutions de versions sur des plateformes Red Hat Enterprise Linux.
* Intégration et support d’application :
* Réalisation de scripts de déploiement.
* Réalisation de documentation.
* Intégration Supervision :
* Etude de l’environnement et mise en place d’une architecture de supervision Mise en place d’une supervision Nagios/Centreon d’un millier de services. Formation des administrateurs à l’interface de Centreon.

### Intégrateur Progiciels - MACIF (LYNT) d’octobre 2009 à mars 2010

* Intégration de progiciels
* Test et réalisations de documentations d’installation.

### Expert système Linux - MAIF (LYNT) de mai 2008 à septembre 2009

* Expertise Linux
* Création du socle linux et documentation.
* Industrialisation des serveurs Linux (RHEL4 et RHEL5).
* Mise en place de serveurs de déploiement PXE. Normalisation et qualification d’applications et de systèmes. Support Niveau 3 et assistance sur les projets majeurs MAIF.

### Ingénieur Système - Ouest-France Multimédia de juillet 2006 à mai 2008

* Expertise Linux
* Mise en oeuvre d’un cluster web pour les sites  [maville.com/ouestfrance.fr](http://maville.com/OuestFrance.fr)  
* Gestion de machines virtuelles sous Windows/Linux dans un environnement ESX 3
* Administration des serveurs web Linux/Windows.
* Création de la nouvelle plate-forme  [maville.com](http://maville.com/)  (3 millions de visiteurs/mois). 4 serveurs Red Hat en cluster, gestion de Fail-Over, répartition de charge et RAID réseau.
* Etude et mise en production d’une plateforme « Single Sign On », basée sur une plateforme Linux (Apache Tomcat, Annuaire LDAP).
* Supervision et suivit journalier des systèmes.
* Recherche et Développement.

### Administrateur Système et Réseau - Telintrans de Janvier 2001 à juin 2006

* Production/Administration
* Administration des serveurs Novell 4.11, Windows 2000, Linux Red hat 7.3 Supervision des serveurs et services.
* Etude
* Réalisation de maquettes et plans de déploiement pour la mise en place de serveurs de fichiers et d'annuaire Open Enterprise Serveur.
* Recherche et développement.

***

### Formations & Certification

* 2016 Formation Docker
* 2015 Formation AWS
* 2011 Certification Red Hat Certified Engineer
* 2011 Certification Red hat Certified System Administrator
* 2011 Certification ITIL Foundation V3
* 2006 Formation PHP/MySQL Avancée. AIS Formation, Nantes
* 2004 Formation Administration Netware Avancée, Azlan
* 1999 Technicien de Maintenance Informatique. CCI, Le Mans
* 1995 BEP Electronique. IL Combrée
