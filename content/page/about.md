---
title: A propos
description: Qui je suis ? Qu'est ce que je fais ?
menu: main
weight: -210
comments: false

---

## Freddy Grandiere

### Consultant en architectures WEB et DevOPS

Quadra, papa de deux enfants et consultant indépendant après un peu plus de 6 ans d'activité chez un des leaders français de l'hébergement professionnel.

Je propose mes services dans l'infogérence informatique orientée web, les technos DevOPS, Docker, Terraform, Ansible.

Vous pouvez retrouver mon CV en suivant [ce lien](/curriculum-vitae/)

Je pratique aussi la photographie amateur, alors n'hésitez pas à aller faire un tour sur mon [portfolio](http://freddygrandiere.me)
