---
title: "Installer un VPN compatible iOS avec Docker"
date: 2018-03-26T20:00:54+04:00
authors: ["fredgrnd"]
draft: false
tags: ["VPN","Docker"]
---

Avec Docker tout est plus simple, avant pour installer un VPN il fallait passer par tout un tas de commandes, de renseignement de fichier de conf et toutes les règles de firewall qui pouvaient entrer en conflit avec les votre.

Heureusement nous sommes en 2018 et monter un VPN IPsec, en moins de 5 minutes, c'est possible grace à Docker.

Je tiens d’abord à préciser que je n’ai rien inventé, je suis tombé sur ce [GitHub](https://github.com/hwdsl2/docker-ipsec-vpn-server) complètement pas hasard, seulement je trouvais important de partager l’information.

Le seul pré-requis pour que cela fonctionne est d’avoir un serveur qui dispose du module kernel `af_key`.

## Chargement du module `af_key`

```bash
sudo modprobe af_key
```

On vérifie ensuite qu’il est bien chargé :

```bash
sudo lsmod |grep af_key
af_key                 36864  0
xfrm_algo              16384  7 ah4,ah6,esp4,esp6,af_key,xfrm_user,xfrm_ipcomp
```

## Creation de l’arborescence du projet

```bash
mkdir myvpnserver
```

Dans ce dossier nous allons juste  créer un fichier dans lequel nous allons renseigner nos credentials.

* `VPN_IPSEC_PSK`: votre passphrase
* `VPN_USER` : votre login
* `VPN_PASSWORD`: votre password

`vpn.env`

```bash
VPN_IPSEC_PSK=fredgrnd_github_io
VPN_USER=fredgrnd
VPN_PASSWORD=PassW0RD
```

## Démarrage du VPN

Il ne vous reste plus qu’a démarrer votre container

```bash
docker run \
    --name ipsec-vpn-server \
    --env-file ./vpn.env \
    --restart=always \
    -p 500:500/udp \
    -p 4500:4500/udp \
    -v /lib/modules:/lib/modules:ro \
    -d --privileged \
    hwdsl2/ipsec-vpn-server
```

Vous avez surement remarqué que nous avons lancé le container en mode privilégié, ce mode est nécessaire car le container doit pouvoir interagir avec l’hôte.

## Commandes utiles

Voici quelques commandes qui peuvent vous être utile :

Vérifier le status du VPN

```bash
docker exec -it ipsec-vpn-server ipsec status
```

Afficher les connections :

```bash
docker exec -it ipsec-vpn-server ipsec whack --trafficstatus
```

Il ne vous reste plus qu’a configurer votre client, pour cela je vous renvoie directement vers la [doc de l’auteur de cette images](https://github.com/hwdsl2/setup-ipsec-vpn/blob/master/docs/clients.md).
Si sont projet vous intéresse n’hésitez pas à vous abonner à son dépôt.
