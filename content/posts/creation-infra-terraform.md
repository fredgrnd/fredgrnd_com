---
title: "Création d'une infrastructure avec Terraform"
date: 2018-04-17T10:00:04+04:00
authors: ["fredgrnd"]
tags: [ "terraform", "hashicorp", "DevOPS"]
draft: false
---

![Terraform Logo](/images/terraform-color.png)

[**Terraform**](https://www.terraform.io) est un outil open source développé par [**HashiCorp**](https://www.hashicorp.com), une société spécialisée dans l'automatisation et le déploiement d'infrastructure dans le cloud.

Pour faire simple, vous pouvez créer, démarrer, modifier, stopper et détruire une infrastructure complète à l'aide de quelques fichiers et d'une commande depuis votre poste de travail.

Nous allons voir dans cet article son installation et un cas pratique simple, le déploiement d'une infrastructure sur [**DigitalOcean**](https://m.do.co/c/8c2b486659b9) [^1].

## Installation du client

L'installation est relativement simple, il suffit de télécharger le binaire depuis la page de [**Terraform**](https://www.terraform.io/downloads.html) et de le copier à l'endroit souhaité.

Sur macOS ça donne ceci :

```bash
sudo cp ./Downloads/terraform /usr/local/bin/terraform
```

On vérifie que tout est OK en vérifiant la version

```bash
$ terraform -v
Terraform v0.11.4
```

## Déploiement de l'infrastructure

Nous allons donc créer une infrastructure simple via [**Terraform**](https://terraform.io). Elle comprendra les éléments suivants :

* 2 serveurs **Ubuntu** et leurs enregistrements DNS.
* les règles de firewall qui vont bien.

Les accès aux serveurs de [**DigitalOcean**](https://m.do.co/c/8c2b486659b9) [^1] se font via clé **SSH**, si vous en avez déjà une, c'est très bien, sinon il faut la créer. Nous partons aussi du principe que votre domaine est géré par [**DigitalOcean**](https://m.do.co/c/8c2b486659b9) [^1].

### Création de l'arborescence

Nous allons commencer par créer le répertoire pour y stocker tous les fichiers nécessaires à la création de notre serveur. Ce dossier stockera aussi les fichiers essentiels au fonctionnement de [**Terraform**](https://terraform.io).

```bash
mkdir myinfra
```

### Définition du provider

Le provider défini l'hébergeur chez lequel nous allons déployer notre infrastructure. Il doit être compatible avec [**Terraform**](https://terraform.io). Vous avez une liste exhaustive des providers directement sur [le site](https://www.terraform.io/docs/providers/index.html).

`provider.tf`

```yaml
variable "doToken" {}

provider "digitalocean" {
 token = "${var.doToken}"
 }
```

Vous remarquerez que nous utilisons la variable `doToken`. Pour des raisons de sécurité évidentes nous ne mettrons pas directement notre token API dans le fichier de configuration mais plutôt en variable système voir dans un fichier spécifique.

### Definition des serveurs

Dans ce fichier, nous allons mettre toutes les options nécessaires pour démarrer nos serveurs.

`myserver01.tf`

```yaml
resource "digitalocean_droplet" "server01_droplet" {
    image = "ubuntu-16-04-x64"
    name = "server01"
    region = "lon1"
    private_networking = "true"
    size = "s-1vcpu-1gb"
    ssh_keys = [17232273]
}

resource "digitalocean_record" "server01_record" {
  domain = "fredgrnd.com"
  type   = "A"
  name   = "server01"
  value  = "${digitalocean_droplet.server01_droplet.ipv4_address}"
  ttl    = "3600"
}
```

`myserver02.tf`

```yaml
resource "digitalocean_droplet" "server02_droplet" {
    image = "ubuntu-16-04-x64"
    name = "server02"
    region = "lon1"
    private_networking = "true"
    size = "s-1vcpu-1gb"
    ssh_keys = [17232273]
}

resource "digitalocean_record" "server02_record" {
  domain = "fredgrnd.com"
  type   = "A"
  name   = "server02"
  value  = "${digitalocean_droplet.server02_droplet.ipv4_address}"
  ttl    = "3600"
}
```

Nous avons défini deux ressources :

* `digitalocean_droplet` : Les variables qui concernent nos serveurs.
* `digitalocean_record` : Les enregistrements DNS.

### Définition du firewall

Dans ce fichier nous allons définir un firewall avec quelques règles et y mettre les deux serveurs que nous venons de configurer.

`firewall.tf`

```yaml
resource "digitalocean_firewall" "myinfra" {
  name = "myinfra"
  droplet_ids = ["${digitalocean_droplet.server01_droplet.id}", "${digitalocean_droplet.server02_droplet.id}"]

  inbound_rule = [
    {
      protocol           = "tcp"
      port_range         = "22"
      source_addresses   = ["0.0.0.0/0", "::/0"]
    },
    {
      protocol           = "tcp"
      port_range         = "80"
      source_addresses   = ["0.0.0.0/0", "::/0"]
    },
    {
      protocol           = "tcp"
      port_range         = "443"
      source_addresses   = ["0.0.0.0/0", "::/0"]
    },
  ]

}
```

### Initialisation de terraform

Avant de lancer notre serveur, il faut initialiser notre configuration afin que [**Terraform**](https://terraform.io) installe les modules nécessaires au bon fonctionnement de notre nouvelle infrastructure. Il installera, entre autre, le module de [**DigitalOcean**](https://m.do.co/c/8c2b486659b9) [^1] que nous avons défini dans le fichier `provider.tf`.

```bash
terraform init
```

Si vous ne rencontrez pas d'erreur à ce niveau c'est que votre configuration semble bonne

### Planification du déploiement

Avant de lancer quoi que ce soit, nous devons nous assurer que tout est bon. Pour cela nous allons utiliser la commande [**Terraform**](https://terraform.io) plan.

```bash
terraform plan
```

Cette commande permet à [**Terraform**](https://terraform.io) de liste les ressources définies dans les différents fichiers et de créer son plan d'exécution. Elle nous indique aussi  toutes les étapes du processus, cela va nous permettre de vérifier qu'il ne manque rien.
Les étapes marquée comme ``<computed>`` seront gérée automatiquement par [**DigitalOcean**](https://m.do.co/c/8c2b486659b9) [^1].

### Création de l'infrastructure

Nous y sommes, il est temps de lancer notre infrastructure. Pour cela une seul commande pour tout lancer

```bash
terraform apply
```

Il ne vous reste qu'a attendre quelques secondes que vos serveurs finissent de démarrer, votre infra est prête.

### Ajout d'un serveur

Imaginons que notre infra n'est pas suffisamment taillée et qu'il faille ajouter une machine supplémentaire. Pour cela rien de plus simple, il suffit de créer son fichier de définition puis de modifier celui du firewall en rajoutant le serveur.

`myserver03.tf`

```yaml
resource "digitalocean_droplet" "server03_droplet" {
    image = "ubuntu-16-04-x64"
    name = "server03"
    region = "lon1"
    private_networking = "true"
    size = "s-1vcpu-1gb"
    ssh_keys = [17232273]
}

resource "digitalocean_record" "server03_record" {
  domain = "fredgrnd.com"
  type   = "A"
  name   = "server03"
  value  = "${digitalocean_droplet.server03_droplet.ipv4_address}"
  ttl    = "3600"
}
```

`firewall.tf`

```diff
-droplet_ids = ["${digitalocean_droplet.server01_droplet.id}", "${digitalocean_droplet.server02_droplet.id}"]
+droplet_ids = ["${digitalocean_droplet.server01_droplet.id}", "${digitalocean_droplet.server02_droplet.id}", "${digitalocean_droplet.server03_droplet.id}"]
```

Une fois ces ajouts fait il ne reste plus qu'a planifier le déploiement

```bash
terraform plan
```

Puis si tout est OK de mettre à jour notre infrastructure

```bash
terraform apply
```

### Récupération des informations de l'infrastructure

Vous pouvez afficher tous les éléments créés via une simple commande.

```bash
terraform show
```

### Destruction de l'architecture

Dés que vous n'avez plus besoin de votre infra, il ne vous reste qu'a faire le nettoyage. **Tous** les éléments créés seront supprimés.

```bash
terraform destroy
```

## Conclusion

[**Terraform**](https://terraform.io) est un outils simple mais d'une puissance considérable. Nous avons vu dans cet exemple à quel point il était simple de mettre en place une infrastructure. Mais [**Terraform**](https://terraform.io) ne s'arrête pas là, je vous conseil d'aller faire un tour sur leur [documentation](https://www.terraform.io/docs/index.html) ainsi que sur le [Guide de démarrage](https://www.terraform.io/intro/getting-started/install.html)

[^1]: Liens sponsorisé, en vous inscrivant vous recevrez 10$ de crédit et vous aiderez le blog.
