---
title: "Terraform - stocker l'état dans un Storage Account Azure"
date: 2019-08-13T09:34:06+02:00
authors: ["fredgrnd"]
tags: [ "terraform", "azure", "DevOPS"]
draft: false
---

Par défaut, Terraform stock son état dans un fichier plat à la racine du projet. Si c'est la meilleure méthode dans le cas d'une infrastructure éphémère, ça l'est moins dans un cas de production.

Stocker son état dans un objet accessible depuis n'importe où à plusieurs avantages :

* Accès depuis n'importe quel réseau, n'importe quel poste
* Utilisation par tous les membres d'une équipe
* Résilience

Dans cet exemple nous allons stocker l'état dans un Storage Account Azure.

## Définition des variables d'environnement

Afin de faciliter le processus et d'éviter les erreurs de frappes, nous allons exporte nos informations dans des variables d'environnement *(mettez ce que vous voulez)*.

```bash
export AZ_RESOURCEGROUP=terraformStatesRG
export AZ_LOCATION=westeurope
export AZ_STORAGEACCOUNT=terraformState
export AZ_BLOBCONTAINER=myterraformcontainer
```

## Création du Storage Account Azure

Avant toutes choses nous allons créer le groupe de ressource qui accueillera notre Storage Account

```bash
az group create --name $AZ_RESOURCEGROUP --location $AZ_LOCATION
```

Nous allons maintenant créer le storage account

```bash
az storage account create \
    --name $AZ_STORAGEACCOUNT \
    --resource-group $AZ_RESOURCEGROUP \
    --location $AZ_LOCATION \
    --sku Standard_RAGRS \
    --kind StorageV2
```

Il ne reste plus qu'à créer le container

```bash
az storage container create --name $AZ_BLOBCONTAINER
```

## Configurer Terraform pour utiliser le storage Account plutôt qu'une fichier JSON

### Configuration du backend

Dans votre script terraform, insérez le code suivant:

```tf
terraform {
required_version = "~> 0.12"
  backend "azurerm" {
    storage_account_name = "terraformState"
    container_name = "myterraformcontainer"
    key = "myterraformexample.tfstate"
  }
}
```

### Initialisation de terraform

Afin d'autoriser Terraform à accéder au Storage Account nous allons exporter la "secretKey" du Storage Account dans une variable d'environnement pour l'utiliser ultérieurement

```bash
export AZ_CONTAINERSECRETKEY=$(az storage account keys list \
  -g $AZ_RESOURCEGROUP \
  -n $AZ_STORAGEACCOUNT \
   --query "[0].{value:value}" --output tsv)
```

Puis initialiser Terraform avec cette clé

```bash
terraform init --backend-config="secretkey=$AZ_CONTAINERSECRETKEY"
```

> vous pouvez renseigner directement les paramètres **storage_account_name**, **container_name** et **key** dans la commande `terraform init` en utilisant `--backend-config` comme nous l'avons fait pour **SecretKey**

## Déploiement de l'infrastructure

Reste plus qu'a déployer votre infrastructure

```bash
terraform apply
```
