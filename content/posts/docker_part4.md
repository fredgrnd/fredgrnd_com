---
title: "Docker - Docker Compose"
date: 2018-03-28T16:00:00+04:00
authors: ["fredgrnd"]
draft: false
tags: ["DevOPS","Docker"]
---
![Docker Logo](/images/docker_logo.png)

## Docker Compose

### Qu'est-ce que Docker Compose

**Docker Compose** est un outil pour définir et exécuter des applications Docker multi-conteneur. Avec **Docker Compose**, vous utilisez un fichier YAML pour configurer les services de votre application. Ensuite, avec une seule commande, vous créez et démarrez tous les services à partir de votre configuration.

L'utilisation de **Docker Compose** se fait en 2 étapes:

1. Définissez les services qui composent votre application dans *docker-compose.yml* afin qu'ils puissent être exécutés ensemble dans un environnement isolé.
2. Lancer votre application avec **docker-compose**.

Les commandes de Docker Compose permettent de gérer l'application:

* Démarrer, arrêter et reconstruire des services
* Afficher l'état des services en cours d'exécution
* Diffuser la sortie du journal des services en cours d'exécution
* Exécuter une commande unique sur un service

### Qu'est-ce qu'un fichier docker-compose.yml

Le fichier *docker-compose.yml* centralise toutes les informations sur les ressources de votre application. A lui seul ce fichier permet de lancer un application complète, en toute autonomie et sur n'importe quelle plateforme **Docker**.

Il se compose comme suit :

```yaml
version: '3'
services:
  web:
    build: .
    ports:
    - "5000:5000"
    volumes:
    - .:/code
    - logvolume01:/var/log
    links:
    - redis
  redis:
    image: redis
volumes:
  logvolume01:
```

### Docker Compose CLI

Vous pouvez retrouver le référentiel de toutes les commandes docker-compose dans la documentation officielle: [Overview of docker-compose CLI](https://docs.docker.com/compose/reference/overview/)

#### [docker-compose up](https://docs.docker.com/compose/reference/up/)

Construit, créé et démarre l'application

##### Exemple: Démarre l'application a l'aide du docker-compose.yml situé à la racine du répertoire

```bash
docker-compose -d up
Creating network "mycontainer01_default" with the default driver
Pulling mycontainer01 (skydiverss/linux_workstation:latest)...
latest: Pulling from skydiverss/linux_workstation
1b39978eabd9: Pull complete
f94f6f25d713: Pull complete
f441d8ff8656: Pull complete
4cc7cbaebc7b: Pull complete
7fb0efbd5075: Pull complete
29b2924406ea: Pull complete
efce53536e1f: Pull complete
b8cfbc08d8da: Pull complete
9760651d1271: Pull complete
df94486d5fd7: Pull complete
edece96b4a40: Pull complete
6b418d44e830: Pull complete
ef595d692a4e: Pull complete
Digest: sha256:ce74339c08a4aa8ee3c1bf5f562a708df68e8a8a4e6deedf592017818655067a
Status: Downloaded newer image for skydiverss/linux_workstation:latest
Creating mycontainer01_mycontainer01_1 ...
Creating mycontainer01_mycontainer01_1 ... done
```

#### docker-compose [start](https://docs.docker.com/compose/reference/start/)/[stop](https://docs.docker.com/compose/reference/stop/)/[restart](https://docs.docker.com/compose/reference/restart/)

Permet de démarrer et stopper une application

*Exemple:*

```bash
docker-compose start
docker-compose stop
docker-compose restart
```

#### [docker-compose top](https://docs.docker.com/compose/reference/top/)

Afficher les process démarré dans un conteneur

*Exemple:*

```bash
docker-compose top
mycontainer01_mycontainer01_1
PID    USER   TIME        COMMAND
--------------------------------------
3338   root   0:00   /usr/sbin/sshd -D
```

#### [docker-compose logs](https://docs.docker.com/compose/reference/logs/)

Affiche les logs de l'application

*Exemple:*

```bash
docker-compose logs
Attaching to mycontainer01_mycontainer01_1
```

#### [docker-compose ps](https://docs.docker.com/compose/reference/ps/)

Affiche la liste des conteneurs

*Exemple:*

```bash
docker-compose ps
         Name                        Command                 State             Ports
--------------------------------------------------------------------------------------------
mywordpress_db_1          docker-entrypoint.sh mysqld      Up           3306/tcp
mywordpress_wordpress_1   /entrypoint.sh apache2-for ...   Restarting   0.0.0.0:8000->80/tcp
```

#### [docker-compose rm](https://docs.docker.com/compose/reference/rm/)

Supprime les conteneurs d'une application

*Exemple:*

```bash
docker-compose rm
Going to remove djangoquickstart_web_run_1
Are you sure? [yN] y
Removing djangoquickstart_web_run_1 ... done
```

#### [docker-compose down](https://docs.docker.com/compose/reference/down/)

Stop et supprime les conteneurs, volume, réseaux et images créés via **docker-compose**.
*Exemple: Stop et supprime les conteneurs et réseau (par défaut)*

```bash
docker-compose down
Stopping dockerwordpress_web_1 ... done
Stopping dockerwordpress_db_1  ... done
Removing dockerwordpress_web_1 ... done
Removing dockerwordpress_db_1  ... done
Removing network dockerwordpress_default
```

##### Exemple: Stop et supprime les conteneurs, le réseau et les volumes

```bash
docker-compose down -v
Stopping dockerwordpress_web_1 ... done
Stopping dockerwordpress_db_1  ... done
Removing dockerwordpress_web_1 ... done
Removing dockerwordpress_db_1  ... done
Removing network dockerwordpress_default
Removing volume dockerwordpress_db_data
```

##### Exemple: Stop et supprime les conteneurs, le réseau et les images, mais garde les volumes

```bash
docker-compose down --rmi all
Stopping dockerwordpress_web_1 ... done
Stopping dockerwordpress_db_1  ... done
Removing dockerwordpress_web_1 ... done
Removing dockerwordpress_db_1  ... done
Removing network dockerwordpress_default
Removing image mysql:5.7
Removing image wordpress:latest
```

### [Atelier] Mise en œuvre d'un LAMP Wordpress avec docker compose

#### Création d'un ensemble de conteneur Wordpress et MySQL et d'un volume persistant

Nous allons renseigner les différents éléments qui constituerons notre LAMP dans notre fichier *docker-compose.yml*

```yaml
version: '3'

services:
   db:
     image: mysql:5.7
     volumes:
       - db_data:/var/lib/mysql
     restart: always
     environment:
       MYSQL_ROOT_PASSWORD: P0werLin
       MYSQL_DATABASE: wordpress
       MYSQL_USER: wordpress
       MYSQL_PASSWORD: wordpress

   web:
     depends_on:
       - db
     image: wordpress:latest
     ports:
       - "8000:80"
     restart: always
     environment:
       WORDPRESS_DB_HOST: db:3306
       WORDPRESS_DB_USER: wordpress
       WORDPRESS_DB_PASSWORD: wordpress
volumes:
    db_data:
```

Nous avons dans notre exemple, 2 services (db et web) et un volume (db_data).

##### db

* `image: mysql:5.7`.
* `volume: db_data` qui sera monté dans le répertoire /var/lib/mysql du conteneur.
* `restart: always` ici on spécifie une restart automatique en cas de plantage
* `environment:` ici les variables d'environnement nécessaire au fonctionnement de l'application

##### web

* `depends_on: web` ne sera démarré que lorsque le service **db** sera UP.
* `image:` on prends ici une image de wordpress toute prête et maintenue
* `port:` les ports exposé sur l'hôte
* `restart: always` ici on spécifie une restart automatique en cas de plantage
* `environment:` ici les variables d'environnement nécessaire au fonctionnement de l'application

##### db_data

* volume de données persistante

#### Création de l'arborescence, démarrage et manipulation du conteneur

Création de l'arborescence comme suit:

```bash
docker_wordpress/
  - docker-compose.yml
```

Démarrage de l'application

```bash
docker compose up -d
Creating network "dockerwordpress_default" with the default driver
Creating volume "dockerwordpress_db_data" with default driver
Pulling db (mysql:5.7)...
5.7: Pulling from library/mysql
85b1f47fba49: Already exists
5671503d4f93: Pull complete
3b43b3b913cb: Pull complete
4fbb803665d0: Pull complete
05808866e6f9: Pull complete
1d8c65d48cfa: Pull complete
e189e187b2b5: Pull complete
02d3e6011ee8: Pull complete
d43b32d5ce04: Pull complete
2a809168ab45: Pull complete
Digest: sha256:1a2f9361228e9b10b4c77a651b460828514845dc7ac51735b919c2c4aec864b7
Status: Downloaded newer image for mysql:5.7
Pulling web (wordpress:latest)...
latest: Pulling from library/wordpress
85b1f47fba49: Already exists
d8204bc92725: Pull complete
92fc16bb18e4: Pull complete
31098e61b2ae: Pull complete
f6ae64bfd33d: Pull complete
003c1818b354: Pull complete
a6fd4aeb32ad: Pull complete
a094df7cedc1: Pull complete
e3bf6fc1a51d: Pull complete
ad235c260360: Pull complete
edbf48bcbd7e: Pull complete
fd6ae81d5745: Pull complete
69838fd876d6: Pull complete
3186ebffd72d: Pull complete
b24a415ea2c0: Pull complete
225bda14ea90: Pull complete
fc0ad3550a92: Pull complete
0e4600933a8c: Pull complete
Digest: sha256:937862438b4f2a6b56193ef2cfd5fe345566e51278be56a04a7dfcdde18c5922
Status: Downloaded newer image for wordpress:latest
Creating dockerwordpress_db_1 ...
Creating dockerwordpress_db_1 ... done
Creating dockerwordpress_web_1 ...
Creating dockerwordpress_web_1 ... done
```

Vérification du bon fonctionnement de notre application

```bash
curl -I http://localhost:8000
```

#### Finalisation de l'installation de wordpress via un navigateur web

Il ne vous reste qu'a vous connecter depuis votre navigateur pour terminer l'Installation. Lorsque l'on vous demandera les infos de connections, n'oubliez pas que vous les avez défini dans le fichier `docker-compose.yml` dans notre cas l'hôte MySQL s'appelle `db`.

Affichage des conteneurs démarrés

```bash
docker_wordpress docker-compose ps
        Name                       Command               State          Ports
-------------------------------------------------------------------------------------
dockerwordpress_db_1    docker-entrypoint.sh mysqld      Up      3306/tcp
dockerwordpress_web_1   docker-entrypoint.sh apach ...   Up      0.0.0.0:8000->80/tcp
```

## Cet article fait partie d'une série sur Docker

* [Docker - Présentation]({{< ref "docker_part1.md" >}})
* [Docker - Premiers pas]({{< ref "docker_part2.md" >}})
* [Docker - Creation d'une image]({{< ref "docker_part3.md" >}})

[^1]: source [Docker](https://www.docker.com/)
