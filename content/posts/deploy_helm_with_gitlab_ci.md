---
title: "Déployer une charte Helm grace à Gitlab-CI"
date: 2019-12-20T10:00:04+04:00
authors: ["fredgrnd"]
tags: [ "gitlab", "helm", "DevOPS"]
draft: true
---

[Helm](https://helm.sh) à grandement simplifié le déploiement d'application sur [Kubernetes](https://kubernetes.io/fr/). Afin d'aller encore plus loin dans l'automation du cluster [AKS](https://azure.microsoft.com/fr-fr/services/kubernetes-service/) d'un de mes clients j'ai mis en place une CI Gitlab qui s'occupe de mettre à jour les changement apporté dans la chart Helm de l'application [Sentry](https://sentry.io/).

Dans cet exemple je ne vais pas m'attarder sur Gitlab-CI et Helm mais uniquement sur la CI en elle même. Si vous souhaitez approfondir vos connaissance je vous invite à vous rendre sur les documentations:

* [Gitlab-CI](https://docs.gitlab.com/ce/ci/)
* [Helm](https://v2.helm.sh/docs/)

## Variables

Afin de rendre le code plus facile à réutiliser, j'utilise les variables au maximum. Les variables sensibles seront renseignée dans GitLab, les autres seront dans le fichier de la CI `.gitlab-ci.yml`

Les variables à déclarer dans Gitlab:

* `SMTP_USERNAME`
* `SMTP_PASSWORD`
* `REDIS_PASSWORD`
* `POSTGRESQL_PASSWORD`
* `S3_SECRETKEY`
* `K8S_CONFIG`: Le contenu du fichier .kube/config encodé en base64

```yaml
variables:
  KUBECONFIG: kubeconfig
  NAMESPACE: sentry
  CHART: stable/sentry
  RELEASE: sentry
```

## Docker In Docker

Mes runners Gitlab sont sur Docker, je dois donc déclarer le service Docker in Docker.

```yaml
services:
  - docker:dind
```

## Les 3 étapes du déploiement

### Configurer la connection à Kubernetes*

```bash
echo ${K8S_CONFIG} | base64 -d > ${KUBECONFIG}
```

### Initialiser Helm sans installer Tiller

```bash
helm init --client-only
```

### Déployer la chart

```bash
helm upgrade --debug --install --set email.user=${SMTP_USERNAME},email.password=${SMTP_PASSWORD},redis.password=${REDIS_PASSWORD},postgresql.postgresqlPassword=${POSTGRESQL_PASSWORD},filestore.s3.secretKey=${S3_SECRETKEY} --dry-run -f ./values.yml --namespace ${NAMESPACE} $RELEASE ${CHART}
```

Remarquez le `--install`, cela permet d'installer la chart si celle-ci n'est pas présente au moment du déploiement.

## Stages

J'ai défini 2 stages, le premier fera un *"dry-run"* le second fera le déploiement, ces deux stages sont identiques, les actions sont juste lancées avec l'option `--dry-run`
Helm ne fourni pas sa propre image Docker, j'ai donc utilisée celle de [Alpine Linux](https://hub.docker.com/r/alpine/helm).

```yaml
stages:
  - dryrun
  - deploy

dryrun:
  stage: dryrun
  image:
    name: alpine/helm:2.16.1
    entrypoint: [""]
  before_script:
    - echo ${K8S_CONFIG} | base64 -d > ${KUBECONFIG}
  script:
    - helm init --client-only
    - helm upgrade --debug --install --set email.user=${SMTP_USERNAME},email.password=${SMTP_PASSWORD},redis.password=${REDIS_PASSWORD},postgresql.postgresqlPassword=${POSTGRESQL_PASSWORD},filestore.s3.secretKey=${S3_SECRETKEY} --dry-run -f ./values.yml --namespace ${NAMESPACE} $RELEASE ${CHART}

deploy:
  stage: deploy
  image:
    name: alpine/helm:2.16.1
    entrypoint: [""]
  before_script:
    - echo ${K8S_CONFIG} | base64 -d > ${KUBECONFIG}
  script:
    - helm init --client-only
    - helm upgrade --debug --install --set email.user=${SMTP_USERNAME},email.password=${SMTP_PASSWORD},redis.password=${REDIS_PASSWORD},postgresql.postgresqlPassword=${POSTGRESQL_PASSWORD},filestore.s3.secretKey=${S3_SECRETKEY} -f ./values.yml --namespace ${NAMESPACE} $RELEASE ${CHART}
  only:
    refs:
      - master
```

## Le fichier au complet

```yaml
variables:
  KUBECONFIG: kubeconfig
  NAMESPACE: sentry
  CHART: stable/sentry
  RELEASE: sentry

services:
  - docker:dind

stages:
  - dryrun
  - deploy

dryrun :
  stage: dryrun
  image:
    name: alpine/helm:2.16.1
    entrypoint: [""]
  before_script:
    - echo ${K8S_CONFIG} | base64 -d > ${KUBECONFIG}
  script:
    - helm init --client-only
    - helm upgrade --debug --install --set email.user=${SMTP_USERNAME},email.password=${SMTP_PASSWORD},redis.password=${REDIS_PASSWORD},postgresql.postgresqlPassword=${POSTGRESQL_PASSWORD},filestore.s3.secretKey=${S3_SECRETKEY} --dry-run -f ./values.yml --namespace ${NAMESPACE} $RELEASE ${CHART}

deploy:
  stage: deploy
  image:
    name: alpine/helm:2.16.1
    entrypoint: [""]
  before_script:
    - echo ${K8S_CONFIG} | base64 -d > ${KUBECONFIG}
  script:
    - helm init --client-only
    - helm upgrade --debug --install --set email.user=${SMTP_USERNAME},email.password=${SMTP_PASSWORD},redis.password=${REDIS_PASSWORD},postgresql.postgresqlPassword=${POSTGRESQL_PASSWORD},filestore.s3.secretKey=${S3_SECRETKEY} -f ./values.yml --namespace ${NAMESPACE} $RELEASE ${CHART}
  only:
    refs:
      - master

```
