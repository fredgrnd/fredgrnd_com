---
title: "Docker - Présentation"
date: 2018-03-25T16:00:00+04:00
authors: ["fredgrnd"]
draft: false
tags: ["DevOPS","Docker"]
---
![Docker Logo](/images/docker_logo.png)

## Présentation de Docker

### Les différentes versions de Docker

Disponible sur tous les environnements, Linux, Windows, Mac, AWS, Azure

#### Docker CE (Community Edition)

*Docker Community Edition (CE) est idéal pour les développeurs et les petites équipes qui souhaitent démarrer avec Docker et expérimenter des applications basées sur des conteneurs. Disponible pour de nombreuses plates-formes d'infrastructure populaires telles que les systèmes d'exploitation desktop, cloud et open source, Docker CE fournit un installateur pour une installation simple et rapide afin que vous puissiez commencer à développer immédiatement. Docker CE est intégré et optimisé pour l'infrastructure, ce qui vous permet de maintenir une expérience d'application native tout en commençant à utiliser Docker. Créez le premier conteneur, partagez avec les membres de l'équipe et automatisez le pipeline dev, le tout avec Docker Community Edition.* [^1]

#### Docker EE (Enterprise Edition)

*Docker Enterprise Edition (EE) est une plate-forme de gestion des conteneurs en tant que service qui gère et sécurise diverses applications à travers une infrastructure disparate, sur site et dans le cloud. Docker EE alimente l'innovation en regroupant les applications traditionnelles et les micro services basés sur Windows, Linux ou Linux-on-mainframe en une seule chaîne d'approvisionnement logicielle sécurisée. Avec Docker, les organisations peuvent moderniser les applications, l'infrastructure et les modèles opérationnels en mettant en avant les investissements informatiques existants tout en intégrant les nouvelles technologies au rythme des activités.* [^1]

#### Les différences entres les versions

| Capabilities | Community Edition | Enterprise Edition Basic | Enterprise Edition Standard | Enterprise Edition Advanced |
|-------------------------------------------------------------------|-------------------|--------------------------|-----------------------------|-----------------------------|
| Container engine and built in orchestration, networking, security | X | X | X | X |
| Certified infrastructure, plugins and ISV containers              |                   | X                        | X                           | X                           |
| Image management                                                  |                   |                          | X                           | X                           |
| Container app management                                          |                   |                          | X                           | X                           |
| Image security scanning                                           |                   |                          |                             | X                           |

### Quelle utilisation

#### Mise en place rapide d'application

Docker permet de mettre en place rapidement une application. Imaginez que vous ayez besoin de tester une fonctionnalité ou une version particulière de MySQL.
Vous pouvez d'une commande lancer un serveur MySQL dans la version que vous souhaitez

#### Portabilité

Installer ce même serveur MySQL sur une station linux est relativement aisé, par contre sur un environnement Windows ou Mac c'est beaucoup plus compliqué. Docker étant multiplateforme, vous pouvez lancer un serveur MySQL sur n'importe quelle station, et de la même façon. Le résultat sera identique.

#### Lancer des applications différentes sur le même hôte

Sur le même hôte Docker, si les ressources le permettent, vous pouvez lancer des applications voir des versions d'application très simplement sans qu'aucun n'interfère avec l'autre. Par exemple il est tout a fait possible d'installer un server MySQL et un serveur MariaDB sur le même hôte.

### L'architecture Docker [^1]

![L'architecture Docker](/images/docker_architecture.png)

#### Docker Daemon [^1]

Le démon Docker (dockerd) est à l'écoute des demandes de l'API Docker et gère les objets Docker tels que les images, les conteneurs, les réseaux et les volumes. Un démon peut également communiquer avec d'autres démons pour gérer les services Docker.

#### Docker Client[^1]

Le client Docker (docker) est le principal moyen utilisé par de nombreux utilisateurs Docker pour interagir avec Docker. Lorsque vous utilisez des commandes telles que docker run, le client envoie ces commandes à dockerd, qui les exécute. La commande docker utilise l'API Docker. Le client Docker peut communiquer avec plusieurs démons.

#### Registry[^1]

Un registre Docker stocke les images Docker. Docker Hub et Docker Cloud sont des registres publics que tout le monde peut utiliser, et Docker est configuré pour rechercher des images sur Docker Hub par défaut. Vous pouvez même exécuter votre propre registre privé. Si vous utilisez Docker Datacenter (DDC), il inclut Docker Trusted Registry (DTR).

Lorsque vous utilisez les commandes d'extraction de docker ou d'exécution de docker, les images requises sont extraites de votre registre configuré. Lorsque vous utilisez la commande docker push, votre image est transmise au registre configuré.

Le magasin Docker vous permet d'acheter et de vendre des images Docker ou de les distribuer gratuitement. Par exemple, vous pouvez acheter une image Docker contenant une application ou un service auprès d'un fournisseur de logiciels et utiliser l'image pour déployer l'application dans vos environnements de test, de stockage intermédiaire et de production. Vous pouvez mettre à niveau l'application en tirant la nouvelle version de l'image et en redéployant les conteneurs.

#### Objets[^1]

Lorsque vous utilisez Docker, vous créez et utilisez des images, des conteneurs, des réseaux, des volumes, des plug-ins et d'autres objets. Cette section est un bref aperçu de certains de ces objets.

##### images[^1]

Une image est un modèle en lecture seule avec des instructions pour créer un conteneur Docker. Souvent, une image est basée sur une autre image, avec une personnalisation supplémentaire. Par exemple, vous pouvez créer une image basée sur l'image ubuntu, mais installe le serveur Web Apache et votre application, ainsi que les détails de configuration nécessaires à l'exécution de votre application.

Vous pouvez créer vos propres images ou utiliser uniquement celles créées par d'autres et publiées dans un registre. Pour créer votre propre image, vous créez un Dockerfile avec une syntaxe simple pour définir les étapes nécessaires pour créer l'image et l'exécuter. Chaque instruction dans un fichier Docker crée une couche dans l'image. Lorsque vous modifiez le Dockerfile et reconstruisez l'image, seuls les calques qui ont été modifiés sont reconstruits. Cela fait partie de ce qui rend les images si légères, petites et rapides, par rapport aux autres technologies de virtualisation.

##### Containers[^1]

Un conteneur est une instance exécutable d'une image. Vous pouvez créer, démarrer, arrêter, déplacer ou supprimer un conteneur à l'aide de l'API Docker ou de l'interface de ligne de commande. Vous pouvez connecter un conteneur à un ou plusieurs réseaux, lui associer un stockage ou même créer une nouvelle image en fonction de son état actuel.

Par défaut, un conteneur est relativement bien isolé des autres conteneurs et de sa machine hôte. Vous pouvez contrôler l'isolement du réseau, du stockage ou d'autres sous-systèmes sous-jacents d'un conteneur à partir d'autres conteneurs ou de la machine hôte.

Un conteneur est défini par son image ainsi que toutes les options de configuration que vous lui fournissez lorsque vous le créez ou le démarrez. Lorsqu'un conteneur est supprimé, les modifications de son état qui ne sont pas stockées dans le stockage persistant disparaissent.

##### services[^1]

Les services vous permettent de mettre à l'échelle des conteneurs sur plusieurs démons Docker, qui fonctionnent tous comme un essaim avec plusieurs gestionnaires et employés. Chaque membre d'un essaim est un démon Docker, et les démons communiquent tous en utilisant l'API Docker. Un service vous permet de définir l'état souhaité, tel que le nombre de répliques du service qui doit être disponible à un moment donné. Par défaut, le service est équilibré en charge sur tous les nœuds de travail. Pour le consommateur, le service Docker semble être une application unique. Docker Engine prend en charge le mode Essaim dans Docker 1.12 et supérieur.

### Disponibilité et installation de Docker sur différentes plateformes (Windows, Mac et Linux)

* Intégration Windows Desktop et Server.
* Intégration macOS.
* Intégration Linux. Docker est disponible pour la majorité des distributions *Linux (CentOS, Debian, Fedora, RHEL, SLES, Ubuntu et Oracle Linux)*

## Cet article fait partie d'une série sur Docker

* [Docker - Premiers Pas]({{< ref "docker_part2.md" >}})
* [Docker - Creation d’une image]({{< ref "docker_part3.md" >}})
* [Docker - Docker Compose]({{< ref "docker_part4.md" >}})

[^1]: source [Docker](https://www.docker.com/)
