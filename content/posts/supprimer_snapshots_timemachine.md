---
title: "Libérer rapidement de l’espace en supprimant les snapshots TimeMachine"
date: 2018-05-18T08:48:30+04:00
draft: false
authors: ["fredgrnd"]
tags: [ "macOS", "QuickTip"]
---
Le site [The Sweet Setup](https://thesweetsetup.com/free-system-storage-macos-high-sierra/) nous expliquait récemment comment supprimer les snapshots **TimeMachine** en utilisant la commande `tmutil`. Mais ils ne sont pas allé au bout des choses, imaginez qui vous ayez 30 snapshots à supprimer... vous n'allez pas les faire un par un ! Je vais rapidement rappeler comment lister et supprimer les snapshots, mais surtout vous donner un commande qui les supprimera tous d'un seul coup.

## Lister les snapshots récents

On va lister les snapshots présent sur `/`

```bash
sudo tmutil listlocalsnapshots /
com.apple.TimeMachine.2018-05-16-164946
...
...
com.apple.TimeMachine.2018-05-18-082950
```

## Supprimer un snapshot

Pour supprimer un snapshot, vous devez spécifier uniquement la date de celui-ci

```bash
sudo tmutil deletelocalsnapshots 2018-05-16-164946
Deleted local snapshot '2018-05-16-164946'
```

## Supprimer tous les snapshots d’un coup

Je vais maintenant vous donner une astuce qui vous permettra de supprimer tous les snapshots en une seule ligne de commande.

```bash
for i in $(sudo tmutil listlocalsnapshots / |awk -F. '/^com.apple.TimeMachine/ {print $4}'); do sudo tmutil deletelocalsnapshots $i; done
Deleted local snapshot '2018-05-17-124411'
...
...
Deleted local snapshot '2018-05-18-082950'
```
