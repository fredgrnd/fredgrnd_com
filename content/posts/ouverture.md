---
title: "Ouverture"
date: 2018-03-21T12:14:52+04:00
authors: ["fredgrnd"]
draft: false
tags: ["3615life"]
---
J’ai décidé il y a quelque temps déjà de reprendre mes activités de blogueur technique. Toujours dans **l’OpenSource** et **Linux**, mais avec un blog plutôt orienté **DevOPS** et toutes les technos qui tournent autour.

![XebiaLabs - Periodic Table of DevOps Tools](/images/devops_periodic_table.jpg)
[*XebiaLabs - Periodic Table of DevOps Tools*](https://xebialabs.com/periodic-table-of-devops-tools/)

J’ai créé ce blog avec  [Hugo](https://gohugo.io/), il est hébergé sur [GitHub](http://github.com/). Si les débuts ont été un peu compliqués, une fois lancé c’est vraiment simple, je peux rédiger mes articles offline sur  [Bear](http://www.bear-writer.com/) puis les importer directement dans Hugo. Et comme le blog est entièrement en html, je n’ai aucun problème de performance. (*KeepItSimple*)

Grace à mon poste d’ingénieur système chez un grand hébergeur, j’ai eu la chance de plonger dans l’univers DevOPS et d’avoir eu des formations sur des technos super intéressantes. Je vais donc orienter mes articles et tutos dans ce sens avec  [docker](https://www.docker.com/), [terraform](https://www.terraform.io/), [doctl](https://github.com/digitalocean/doctl), [consul](https://www.consul.io/), [fabric](http://www.fabfile.org/), [ansible](https://www.ansible.com/) et encore bien d’autres. J’ai déja plein d’idées, notamment des articles sur la plateforme Docker que j’utilise.

Le design et la structure de ce blog sont en pleine structuration donc ne vous étonnez pas de voir des changements. Rien n’est encore sec !

A bientôt pour plein de choses très intéressantes.
