---
title: "Gestion des secrets avec Azure KeyVault"
date: 2019-06-26T13:24:43+02:00
authors: ["fredgrnd"]
tags: ["azure"]
draft: false
---

Gérer les secrets d’une plateforme peut vite tourner au cauchemar, je pense avoir tout vu. Du stockage directement dans le code versionné, dans un fichier *KeePass* au classeur Excel.

Dans ce guide je vais vous exposer ma façon de gérer les mots de passe applicatif, ils seront générés de façon aléatoire et stockés dans un KeyVault Azure. Grace à cela, il sera non seulement accessible depuis n’importe quel client connecté et autorisé à l’affiché mais aussi à l’ensemble des composants Azure de votre infrastructure.

## Préparation

Avant toutes choses, vous devez être connecté à Azure avec le CLI.

```bash
az login
```

vous devez aussi disposer d’un groupe de ressource ou mettre votre KeyVault

```bash
az group create -n "myResourceGroup" -l "westeurope"
```

Afin de rendre plus la mise en place plus simple nous allons mettre toutes les infos en variables d’environnement.

```bash
export VAULTNAME="myKeyVault"
export RESOURCEGROUP="myResourceGroup"
export LOCATION="westeurope"
export SECRETNAME="myPassword"
```

## Le KeyVault Azure

### Création du KeyVault

Nous allons commencer par créer un KeyVault

```bash
az keyvault create --name $VAULTNAME --resource-group $RESOURCEGROUP  --location $LOCATION
```

Pour afficher la liste des KeyVault :

```bash
az keyvault list --resource-group $RESOURCEGROUP -o table
```

### Ajout du secrets dans le KeyVault

Grace à la commande `openssl rand -base64 32` nous allons générer un mot de passe aléatoire et le stocker dans notre keyVault:

```bash
az keyvault secret set --vault-name $VAULTNAME --name $SECRETNAME --value $(openssl rand -base64 32)
```

Pour afficher la liste des secrets :

```bash
az keyvault secret list --vault-name $VAULTNAME
```

```json
[
  {
    "attributes": {
      "created": "2019-06-26T09:22:00+00:00",
      "enabled": true,
      "expires": null,
      "notBefore": null,
      "recoveryLevel": "Purgeable",
      "updated": "2019-06-26T09:22:00+00:00"
    },
    "contentType": null,
    "id": "https://xxx.vault.azure.net/secrets/fluent-passwd",
    "managed": null,
    "tags": {
      "file-encoding": "utf-8"
    }
  },
  {
    "attributes": {
      "created": "2019-06-26T12:26:35+00:00",
      "enabled": true,
      "expires": null,
      "notBefore": null,
      "recoveryLevel": "Purgeable",
      "updated": "2019-06-26T12:26:35+00:00"
    },
    "contentType": null,
    "id": "https://xxx.vault.azure.net/secrets/myPassword",
    "managed": null,
    "tags": {
      "file-encoding": "utf-8"
    }
  }
]
```

Pour afficher un secret :

```bash
az keyvault secret show --name $SECRETNAME --vault-name $VAULTNAME
```

```json
{
  "attributes": {
    "created": "2019-06-26T09:22:00+00:00",
    "enabled": true,
    "expires": null,
    "notBefore": null,
    "recoveryLevel": "Purgeable",
    "updated": "2019-06-26T09:22:00+00:00"
  },
  "contentType": null,
  "id": "https://xxx.vault.azure.net/secrets/myPassword",
  "kid": null,
  "managed": null,
  "tags": {
    "file-encoding": "utf-8"
  },
  "value": "41nM82exp5noH08lpKA8dUFM+MXZElh7VptzhDwbbTU="
}
```

## Secrets et variable d’environnement

Pour attribuer mon secret à une variable d’environnement :

```bash
export myPassword=$(az keyvault secret show --name $SECRETNAME --vault-name $VAULTNAME --query value)
```
